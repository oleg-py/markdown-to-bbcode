# Orbs-style top bar

-> ![Category links picture] [1]

## **[Get the code!] [2]**

### How to configure
I added comments to the most of the easily configurable options describing what they do.
When you see comment like `/* OPTION: SOMETHING */` notice the slash after the asterisk (if it is here).
Options can be toggled by simply adding that one slash to necessary options and removing it from all others
so it becomes like this for disabled option: `/* OPTION: SOMETHING *`
This will turn code for that option into comment so it will be disabled.

### Trivia
- Uses #inlineContent for the panel, so incompatible with other #inlineContent tricks without tweaking
- Uses one @import for font. Remember that imports should all be at the top of your list.
- May cover up copyright section. I added an optional extension to your list
  but it might not be compatible with all styles.
- Codes modified to not get messed up by MAL CSS box.
- Minimalistic: uses exactly one image for all icons. Other effects are done using plain CSS.
- Compatible with latest versions of major browsers. Old Opera version (<= 12) have problem with
  pointer-events so bubbles will pop up when you hover over orbs.
- Top bar modification not included. I may add it later.

[1]: http://i.imgur.com/AxoFnpF.png
[2]: https://gist.github.com/VeriTi/20b6daf950ee5f0ec986
