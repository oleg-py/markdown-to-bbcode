__author__ = 'VeriTi'

import mistune
import lexer
from os import linesep


def unsupported(_=None):
    def raiser(self, *args, **kwargs):
        raise ValueError('Unsupported tag conversion')
    return raiser


class BBRenderer(mistune.Renderer):
    def text(self, text):
        return text # disable escaping

    @staticmethod
    def bbtag(tag, content, arg=None):
        arg_tag = '{}={}'.format(tag, arg) if arg is not None else tag
        return '[{arg_tag}]{content}[/{tag}]'.format(**locals())

    def strikethrough(self, text):
        return self.bbtag('s', text)

    def paragraph(self, text):
        return text.strip(' ') + linesep * 2

    def newline(self):
        return linesep

    def list_item(self, text):
        return '[*]' + text + linesep

    def list(self, body, ordered=True):
        return self.bbtag('list', body) if not ordered else self.bbtag('list', body, '1')

    def link(self, link, title, text):
        return self.bbtag('url', text, link)

    def linebreak(self):
        return '\n'

    def image(self, src, title, text):
        return self.bbtag('img', src)

    def hrule(self):
        return self.bbtag('center', '-' * 150)

    def header(self, text, level, raw=None):
        text = self.bbtag('size', text, 100 + (7 - level) * 20)
        return (self.emphasis if level >= 4 else self.double_emphasis)(text) + '\n'

    @unsupported
    def footnotes(self, text):
        return super().footnotes(text)

    @unsupported
    def footnote_ref(self, key, index):
        return super().footnote_ref(key, index)

    @unsupported
    def footnote_item(self, key, text):
        return super().footnote_item(key, text)

    def emphasis(self, text):
        return self.bbtag('i', text)

    def double_emphasis(self, text):
        return self.bbtag('b', text)

    def codespan(self, text):
        return self.emphasis(text)

    def block_quote(self, text):
        return self.bbtag('quote', text.strip()) + linesep * 2

    @unsupported
    def table_cell(self, content, **flags):
        return super().table_cell(content, **flags)

    @unsupported
    def table_row(self, content):
        return super().table_row(content)

    @unsupported
    def tag(self, html):
        return super().tag(html)

    @unsupported
    def block_html(self, html):
        return super().block_html(html)

    @unsupported
    def table(self, header, body):
        return super().table(header, body)

    def block_code(self, code, lang=None):
        return self.block_quote(code)

    def autolink(self, link, is_email=False):
        return self.bbtag('url', link, link)

    # custom rule
    def center(self, text):
        return self.bbtag('center', text)


def get():
    bb_renderer = BBRenderer()
    block_lexer = lexer.ExtendedBlockLexer()
    markdown = lexer.ExtendedMarkdown(renderer=bb_renderer, escape=False, block=block_lexer)
    return markdown.render