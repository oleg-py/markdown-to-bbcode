from PyQt4.QtGui import *


def create_format(color, *styles):
    qcolor = QColor(color)
    if isinstance(color, str):
        qcolor.setNamedColor(color)
    elif len(color) == 3:
        qcolor.setHsl(*color)
    else:
        raise ValueError("Unexpected value: {}".format(color))
    qformat = QTextFormat()
    qformat.setForeground(qcolor)
    if 'bold' in styles:
        qformat.setProperty(QTextFormat.FontWeight, True)
    if 'italic' in styles:
        qformat.setProperty(QTextFormat.FontItalic, True)
    return qformat


def load_styles():
    return {
        'header': create_format('black', 'bold'),
        'highlight': create_format('red', 'italic'),
        'highlight2': create_format('red', 'bold'),
        'url': create_format('blue', 'italic'),
    }


class MarkdownHighlighter(QSyntaxHighlighter):
    styles = load_styles()

    def __init__(self, document):
        super().__init__(document)

    def highlightBlock(self, p_str):
        pass