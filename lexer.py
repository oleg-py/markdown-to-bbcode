import copy, re
from mistune import *


class ExtendedBlockGrammar(BlockGrammar):
    center = re.compile(r'->\s*([^\n]+)')


class ExtendedBlockLexer(BlockLexer):
    grammar_class = ExtendedBlockGrammar
    default_rules = ['center'] + copy.copy(BlockLexer.default_rules)

    def parse_center(self, m):
        text = m.group(1)
        # noinspection PyUnresolvedReferences
        self.tokens.append({
            'type': 'center',
            'text': text,
        })


class ExtendedMarkdown(Markdown):
    def output_center(self):
        return self.renderer.center(self.inline(self.token['text']))