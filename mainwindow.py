__author__ = 'VeriTi'

import sys

from PyQt4.QtGui import *
from PyQt4.uic import *

import converter


if __name__ == '__main__':
    app = QApplication(sys.argv)
    wnd = loadUi('mainwindow.ui')
    cv = converter.get()
    wnd.input.textChanged.connect(lambda: wnd.output.setPlainText(cv(wnd.input.toPlainText())))
    wnd.show()
    sys.exit(app.exec())

