# VeriTi's scripts for CSS
I wrote a few scripts when developing my layout, which allows you to set random CSS and CSS which depends on currently
opened category. I meant to replace them with [CSG] [csg-topic] running online but MAL ditched me... But that's different
story anyways. So, the scripts:


## [Random CSS][rand-css]
There was a PHP script for random CSS already, but it was tough for most people to do, requiring to set up a hosting and
get some PHP written in a correct way... Meh. I made my simpler:  

-> ![Random CSS screen][rand-img]

Say, you want a random background. Enter this at *Code prefix*:

    body { background-image: "

This goes as *Code suffix*:

    " }

Finally, insert URLs into random values. Click *Add line* to add more options. Simple and non-repetative1

This is how I use it for current list for random quotes: [My Random CSS example][rand-css-my]

## [Category-dependent CSS][cat-css]
There was a way to set different backgrounds depending on your current status, but it required you to remove
*All Anime*, which I found unacceptable. So I made this:  

-> ![Category-dependent CSS screen][cat-img]

This allows you to make different CSS depending on currently selected status. It works roughly by checking what is 
written in visitor's address bar as *status=#*, so there is a *default* option, which should be set to what category
opens when somebody just uses the *Anime list* link. You can change it anytime btw.

This is how I use it to show characters depending on current category: [My Category CSS example][cat-css-my]

### Common notes (read this before using any of scripts)
- Category-dependent CSS works only when @imported from CSS in MAL's edit box. It won't be able to get *status=#*
  otherwise. Random CSS works from everywhere.
- You will be given link to edit your codes. Save it, or you will lose it forever. There is no *restore edit link*
  option.
- Use small codes. This is placed on shared hosting with limited bandwidth, so you want your footprint to be as small
  as possible. You can use @import in both of these scripts, so take advantage of it if it makes sense. I may simply 
  remove codes which are too large without any notice.




[csg-topic]: http://myanimelist.net/forum/?topicid=1322619

[rand-img]: http://i.imgur.com/NfENFRu.png
[rand-css]: http://torn-pages.uk.to/experimental/random_css.php
[rand-css-my]: http://torn-pages.uk.to/experimental/random_css.php?edit_id=5

[cat-img]: http://i.imgur.com/AzZ9l5F.png
[cat-css]: http://torn-pages.uk.to/experimental/section_import.php
[cat-css-my]: http://torn-pages.uk.to/experimental/section_import.php?edit_id=1
